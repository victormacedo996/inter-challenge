# inter-cli

[![PyPI - Version](https://img.shields.io/pypi/v/inter-cli.svg)](https://pypi.org/project/inter-cli)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/inter-cli.svg)](https://pypi.org/project/inter-cli)

-----

## Table of Contents

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install inter-cli
```

## License

`inter-cli` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
